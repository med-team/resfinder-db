Source: resfinder-db
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               kma,
               python3
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/resfinder-db
Vcs-Git: https://salsa.debian.org/med-team/resfinder-db.git
Homepage: https://bitbucket.org/genomicepidemiology/resfinder
Rules-Requires-Root: no

Package: resfinder-db
Architecture: all
Depends: ${misc:Depends}
Recommends: resfinder
Description: ResFinder database is a curated database of acquired resistance genes
 ResFinder identifies acquired antimicrobial resistance genes in total or
 partial sequenced isolates of bacteria.
 .
 ResFinder that uses BLAST for identification of acquired antimicrobial
 resistance genes in whole-genome data. As input, the method can use both
 pre-assembled, complete or partial genomes, and short sequence reads
 from four different sequencing platforms. The method was evaluated on
 1862 GenBank files containing 1411 different resistance genes, as well
 as on 23 de-novo-sequenced isolates.
 .
 This package provides the database needed for resfinder.
